import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent {
  @Input() task = '';
  @Output() changedTask = new EventEmitter<string>();
  @Output() deletedTask = new EventEmitter();
  isFocus = true;

  getFocus(event: Event) {
    if(<HTMLInputElement>event.target) {
      return this.isFocus = !this.isFocus;
    }
    return '';
  }

  getBlur(event: Event) {
    if(<HTMLInputElement>event.target) {
      return this.isFocus = true;
    }
    return '';
  }

  getClassName() {
    const className = this.isFocus ? '' : 'focus';
    return 'input ' +  className;
  }

  changeTask(event: Event) {
    const targetTask = <HTMLInputElement>event.target;
    this.changedTask.emit(targetTask.value);
  }

  deleteTask() {
    this.deletedTask.emit();
  }
}
