import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  arrayTasks = ['Buy milk', 'Walk with dog', 'Do homework'];
  mainTask = '';

  addTask(event: Event) {
    event.preventDefault();
    this.arrayTasks.push(this.mainTask);
    this.mainTask = '';
    return this.arrayTasks;
  }

  changeTextOfTask(index: number, newTask: string) {
    this.arrayTasks[index] = newTask;
  }

  deleteTaskFromPage(index: number) {
    this.arrayTasks.splice(index, 1);
  }
}
